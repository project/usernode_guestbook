THIS MODULE WAS WRITTEN BY MARCO RADEMACHER
Marco.Rademacher XXX web.de (oops, "XXX" should be "@", of course)
LICENSED UNDER GPL
NO WARRENTY - USE AT YOUR OWN RISK
Version: $Id$


Overview
--------

So, you have a usernode. Do you want to make further sense of it? ;)

Having a user a node gives also the possibility to post comments on a user.
Let's use these comments as guestbook entries!

usernode_guestbook is another way of implementing a guestbook functionality
besides the former guestbook module. If you want a guestbook that behaves in
a different way to usual comments and you don't have usernode module installed,
use guestbook. But if you have installed usernode already in order to display
users in views module, and you want your guestbook entries to look and to
behave the very same way as usual comments do, then you should chose this module.
For guestbook entries are stored in the comments table, no database tables
need to be installed and both modules are incompatible to each other.
Everything that works with usual comments will also work with
usernode_guestbook, so you can notify your users of new guestbook entries
if you have subscriptions module installed.

usernode_questbook module shows another section on the user's profile page.
The latest comments appear here and also a comment form for immediate greetings.
Everything is automatically themed as comments are, for your pleasure and for
your users' usability.


Requirements
------------

This module requires Drupal 5.x with modules comment and usernode enabled.


Installation
------------

1. Copy the usernode_guestbook directory containing the
   usernode_guestbook.module to the Drupal sites/all/modules/ directory
   or a similar one.

2. Enable usernode_guestbook in the "Administer > site settings > modules"
   administration screen.

3. Enable usernode_guestbook block you want in the
   "Administer > Site settings > Blocks" administration screen.

4. Apply usernode_guestbook settings on the
   "Administer > Site configuration > Usernode guestbook" page.

5. Set comment options on admin/content/comment/settings page:
   * Preview comment: (ignored)
   * Default display mode: Flat list - expanded (recommended)
   * Default display order: Date - newest first (recommended)
   * Location of comment submission form: Display on separate page
     (strongly recommended)


Configuration
-------------

Admins can decide whether an additional guestbook tab appears along with a
user's profile page. They decide about guestbook default activation
which is the same as usual comments on nodes of type usernode.
Default settings can be applied even on already registered users.
Usernode guestbooks are controlable by each user,
even if access to the usernode itself is restricted.

The name of the guestbook is solely translateble. So you can translate it
differently to call it something else using locale module.

Though it is a nice idea to have guestbook entries the same way comments are,
you maybe want to disable some features that come with comments.
Currently, I see most solutions on theming level only.


Further development
-------------------

usernode_guestbook module is still in development. APIs may still change.
First stable version not before drupal 6, so there will be no 4.7 version
of this module. You are invited to take part in the development process.
Feel free to discuss features and post patches. If you would like to
contribute to this module, please submit your ideas and your code at
http://drupal.org/project/issues/usernode_guestbook
It is planned to keep this module a small and useful addition to usernode.

 * Features:
 * - Shows guestbook in user-profile (hook_user 'view')
 * - Shows comment-form in user-profile (hook_user 'view')
 * - Themes guestbook-entries the same way as other comments (REALLY hard work, haha!)
 * - Touches usernode if guestbook is watched to unset new status of viewed comments (node_tag_new)
 * - Guestbook tab additional to user profiles (_usernode_guestbook_tab)
 * - Admin settings page for default activation and appearence (_usernode_guestbook_admin_settings)
 * - Jump directly to the guestbook-section (hook_link 'fragment')
 * - .pot-file
 * - German informal translation
 * - User can switch own guestbook functionality like comments (disable, read only, read and write)
 *   even if usernode access is restricted (hook_user 'form')
 *
 * TODO:
 * - Notification by e-mail on new entries (subscriptions.module?)
 * - README file explains install and gives config advice (comments chrono, subscriptions, etc)
 * - Get rid of doubled formatting of the guestbook entry in preview mode to enable previewing again
 * - Create an own, themable guestbook form that uses comment form theming by default
 * - Show only a smaller number of latest guestbook entries on profile page
 * - Put form at the end of the entries depending on comment order
 *
 * TODO in version 6:
 * - If future hook_link_alter also refers to links on comments,
 *   unset 'reply' Link for usernode guestbook
 * - Make the usernode_guestbook block an independent jQueryly solution

Any suggestions? Wish to help? Contact me! (EMail above)